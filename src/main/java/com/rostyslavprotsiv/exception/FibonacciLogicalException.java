/*
 * FibonacciLogicalException class
 *
 * Version: 1.0.1
 *
 * Created by Rostyslav Protsiv, Date: 06.08.2021
 *
 * Copyright protected
 */

package com.rostyslavprotsiv.exception;

/**
 * {@code FibonacciLogicalException} is a class that represents the situations that
 * should be avoided from the logical side of the Fibonacci class
 *
 * @version 1.0.1
 * @author Rostyslav Protsiv
 * @since 06.08.2021
 */
public class FibonacciLogicalException extends FibonacciException {
    public FibonacciLogicalException() {
    }

    /**
     * <h1>Constructor</h1>
     * @param message The text to be displayed as error reason
     * @param cause The class object that caused exception
     */
    public FibonacciLogicalException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * <h1>Constructor</h1>
     * @param message The text to be displayed as error reason
     */
    public FibonacciLogicalException(String message) {
        super(message);
    }

    /**
     * <h1>Constructor</h1>
     * @param cause The class object that caused exception
     */
    public FibonacciLogicalException(Throwable cause) {
        super(cause);
    }
}
