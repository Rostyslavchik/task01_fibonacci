/*
 * RangeTechnicalException class
 *
 * Version: 1.0.1
 *
 * Created by Rostyslav Protsiv, Date: 06.08.2021
 *
 * Copyright protected
 */

package com.rostyslavprotsiv.exception;

/**
 * {@code RangeTechnicalException} is a class that represents the situations that
 * should be avoided from the technical side of the Range class (some runtime exceptions or errors
 * or smth like this)
 *
 * @version 1.0.1
 * @author Rostyslav Protsiv
 * @since 06.08.2021
 */
public class RangeTechnicalException extends RangeException {
    public RangeTechnicalException() {
    }

    /**
     * <h1>Constructor</h1>
     * @param message The text to be displayed as error reason
     * @param cause The class object that caused exception
     */
    public RangeTechnicalException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * <h1>Constructor</h1>
     * @param message The text to be displayed as error reason
     */
    public RangeTechnicalException(String message) {
        super(message);
    }

    /**
     * <h1>Constructor</h1>
     * @param cause The class object that caused exception
     */
    public RangeTechnicalException(Throwable cause) {
        super(cause);
    }
}
