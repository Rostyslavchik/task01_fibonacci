/*
 * FibonacciException class, superclass of hierarchy of exceptions for Fibonacci class
 *
 * Version: 1.0.1
 *
 * Created by Rostyslav Protsiv, Date: 06.08.2021
 *
 * Copyright protected
 */

package com.rostyslavprotsiv.exception;

/**
 * {@code FibonacciException} class is main class in hierarchy of
 *  exceptions that are used in Fibonacci class
 *
 * @version 1.0.1
 * @author Rostyslav Protsiv
 * @since 06.08.2021
 */
public class FibonacciException extends Exception {
    public FibonacciException() {}

    /**
     * <h1>Constructor</h1>
     * @param message The text to be displayed as error reason
     * @param cause The class object that caused exception
     */
    public FibonacciException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * <h1>Constructor</h1>
     * @param message The text to be displayed as error reason
     */
    public FibonacciException(String message) {
        super(message);
    }

    /**
     * <h1>Constructor</h1>
     * @param cause The class object that caused exception
     */
    public FibonacciException(Throwable cause) {
        super(cause);
    }
}
