/*
 * RangeLogicalException class
 *
 * Version: 1.0.1
 *
 * Created by Rostyslav Protsiv, Date: 06.08.2021
 *
 * Copyright protected
 */
package com.rostyslavprotsiv.exception;

/**
 * {@code RangeLogicalException} is a class that represents the situations that
 * should be avoided from the logical side of the Range class
 *
 * @version 1.0.1
 * @author Rostyslav Protsiv
 * @since 06.08.2021
 */
public class RangeLogicalException extends RangeException {
    /* Some comment here about this class */

    public RangeLogicalException() {}

    /**
     * <h1>Constructor</h1>
     * @param message The text to be displayed as error reason
     * @param cause The class object that caused exception
     */
    public RangeLogicalException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * <h1>Constructor</h1>
     * @param message The text to be displayed as error reason
     */
    public RangeLogicalException(String message) {
        super(message);
    }

    /**
     * <h1>Constructor</h1>
     * @param cause The class object that caused exception
     */
    public RangeLogicalException(Throwable cause) {
        super(cause);
    }
}
