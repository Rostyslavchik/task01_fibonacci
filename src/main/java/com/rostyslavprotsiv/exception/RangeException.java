/*
 * RangeException class, superclass of hierarchy of exceptions for Range class
 *
 * Version: 1.0.1
 *
 * Created by Rostyslav Protsiv, Date: 06.08.2021
 *
 * Copyright protected
 */
package com.rostyslavprotsiv.exception;

/**
 * {@code RangeException} class is main class in hierarchy of
 *  exceptions that are used in Range class
 *
 * @version 1.0.1
 * @author Rostyslav Protsiv
 * @since 06.08.2021
 */
public class RangeException extends Exception {
    public RangeException() {}

    /**
     * <h1>Constructor</h1>
     * @param message The text to be displayed as error reason
     * @param cause The class object that caused exception
     */
    public RangeException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * <h1>Constructor</h1>
     * @param message The text to be displayed as error reason
     */
    public RangeException(String message) {
        super(message);
    }

    /**
     * <h1>Constructor</h1>
     * @param cause The class object that caused exception
     */
    public RangeException(Throwable cause) {
        super(cause);
    }
}
