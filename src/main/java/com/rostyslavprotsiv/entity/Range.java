/*
 * Range class
 *
 * Version: 06.08.2021
 *
 * Created by Rostyslav Protsiv, Date: 06.08.2021
 *
 * Copyright protected
 */
package com.rostyslavprotsiv.entity;

import com.rostyslavprotsiv.exception.RangeLogicalException;

/**
 * <h2>Range</h2>
 * {@code Range} class represents range in math.
 *
 * @version 1.0.1
 * @author Rostyslav Protsiv
 * @since 06.08.2021
 */
public class Range {
    /**
     * <b>Minimal allowed range value</b>
     */
    public static final int MIN_RANGE_VALUE = 0;
    /**
     * <b>Maximal allowed range value</b>
     */
    public static final int MAX_RANGE_VALUE = 200;
    /**
     * <b>Min value</b>
     */
    private int min;
    /**
     * <b>Max value</b>
     */
    private int max;

    public Range() {}

    /**
     * Simple constructor which checking if all is ok with min and max values
     *
     * @param min Minimal range value
     * @param max Maximal range value
     */
    public Range(int min, int max) throws RangeLogicalException {
        if(checkMinMax(min, max)) {
            this.min = min;
            this.max = max;
        } else {
            throw new RangeLogicalException("Min or Max value is not suitable");
        }
    }

    /**
     * Return minimal range value
     * @return Minimal range value
     */
    public int getMin() {
        return min;
    }

    /**
     * Set the new minimal value for range with checking if it is within allowed scope
     * @param min New minimal range value
     */
    public void setMin(int min) throws RangeLogicalException {
        if(checkMinMax(min, max)) {
            this.min = min;
        } else {
            throw new RangeLogicalException("Min value is not suitable");
        }
    }

    /**
     * Return maximal range value
     * @return Maximal range value
     */
    public int getMax() {
        return max;
    }

    /**
     * Set the new maximal value for range with checking if it is within allowed scope
     * @param max New maximal range value
     */
    public void setMax(int max) throws RangeLogicalException {
        if(checkMinMax(min, max)) {
            this.max = max;
        } else {
            throw new RangeLogicalException("Max value is not suitable");
        }
    }

    /**
     * Private method to check all the boundaries of this range
     *
     * @param min Minimal range value
     * @param max Maximal range value
     * @return true, if all the values are in needed scope
     */
    private boolean checkMinMax(int min, int max) {
       return (min < max) && (min >= MIN_RANGE_VALUE) && (max <= MAX_RANGE_VALUE);
    }

    /**
     * You can extend Range class safely with...
     * @return Equality representation of this class
     */
    @Override
    public boolean equals(Object obj) {
        if(this == obj) {
            return true;
        }
        if(obj == null) {
            return false;
        }
        if(getClass() != obj.getClass()) {
            return false;
        }
        Range other = (Range)obj;
        if(min != other.min) {
            return false;
        }
        if(max != other.max) {
            return false;
        }
        return true;
    }

    /**
     * You can extend Range class safely with...
     * @return Int representation of this class
     */
    @Override
    public int hashCode() {
        return (int)(min * 32.45 + max);
    }

    /**
     * You can extend Range class safely with...
     * @return String representation of this class
     */
    @Override
    public String toString() {
        return getClass().getName() + '@' + ",min = " + min + ",max = " + max;
    }
}
