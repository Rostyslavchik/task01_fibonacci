/*
 * Fibonacci class
 *
 * Version: 1.0.1
 *
 * Created by Rostyslav Protsiv, Date: 06.08.2021
 *
 * Copyright protected
 */

package com.rostyslavprotsiv.entity;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * {@code Fibonacci} class was created to represent all
 * Fibonacci numbers
 *
 * @author Rostyslav Protsiv
 * @version 1.0.1
 * @since 06.08.2021
 */
public class Fibonacci {
    private Set<Integer> numbers;
    public static final int MIN_NUMBER = 1;
    public static final int MAX_NUMBER = 30;

    public Fibonacci(int number) {

    }

    private Set generateFibonacciSequence(int number) {
        Set<Integer> newNumbers = new LinkedHashSet<>();
        int previousNumber = 0;
        int currentNumber = 1;
        int temp;
        for(int i = 0; i < number - 1; i ++) {
            newNumbers.add(previousNumber);
            temp = currentNumber;
            currentNumber = currentNumber + previousNumber;
            previousNumber = temp;
        }
        newNumbers.add(previousNumber);
        return newNumbers;
    }


}
