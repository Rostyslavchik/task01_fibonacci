/*
 * RangeAction class
 *
 * Version: 1.0.1
 *
 * Created by Rostyslav Protsiv, Date: 06.08.2021
 *
 * Copyright protected
 */

package com.rostyslavprotsiv.action;

import com.rostyslavprotsiv.entity.Range;

import java.util.Arrays;

/**
 * {@code RangeAction} class was created for executing some operations with Range class
 *
 * @author Rostyslav Protsiv
 * @version 1.0.1
 * @since 06.08.2021
 */
public final class RangeAction {
    /**
     * Private constructor that is supposed to be never used
     */
    private RangeAction() {
        throw new UnsupportedOperationException();
    }

    /**
     * This method returns all odd numbers from the start of range to the end
     * @param range Range of numbers to work with
     * @return Odd numbers from the start of range to the end
     */
    public static int[] getOddFromStartToEnd(Range range) {
        int numOfOddNumbers = (range.getMax() - range.getMin()) / 2 + 1;
        int realSize = 0;
        int[] result = new int[numOfOddNumbers];
        for(int i = range.getMin(); i <= range.getMax(); i ++) {
            if(i % 2 != 0) {
                result[realSize++] = i;
            }
        }
        if(realSize != numOfOddNumbers) {
            result = Arrays.copyOfRange(result, 0, realSize);
        }
        return result;
    }

    /**
     * This method returns all even numbers from the end of range to the start
     * @param range Range of numbers to work with
     * @return Even numbers from the end of range to the start
     */
    public static int[] getEvenFromEndToStart(Range range) {
        int numOfEvenNumbers = (range.getMax() - range.getMin()) / 2 + 1;
        int realSize = 0;
        int[] result = new int[numOfEvenNumbers];
        for(int i = range.getMax(); i >= range.getMin(); i --) {
            if(i % 2 == 0) {
                result[realSize++] = i;
            }
        }
        if(realSize != numOfEvenNumbers) {
            result = Arrays.copyOfRange(result, 0, realSize);
        }
        return result;
    }

    /**
     * This method calculates and returns the sum of all odd numbers
     * @param range The range to work with
     * @return Sum of all odd numbers
     */
    public static int getOddSum(Range range) {
        int[] allOddNumbers = getOddFromStartToEnd(range);
        int sum = 0;
        for(Integer number: allOddNumbers) {
            sum += number;
        }
        return sum;
    }

    /**
     * This method calculates and returns the sum of all even numbers
     * @param range The range to work with
     * @return Sum of all even numbers
     */
    public static int getEvenSum(Range range) {
        int[] allEvenNumbers = getEvenFromEndToStart(range);
        int sum = 0;
        for(Integer number: allEvenNumbers) {
            sum += number;
        }
        return sum;
    }
}
