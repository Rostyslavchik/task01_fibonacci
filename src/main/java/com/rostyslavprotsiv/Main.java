/*
 * Main class
 *
 * Version: 1.0.1
 *
 * Created by Rostyslav Protsiv, Date: 06.08.2021
 *
 * Copyright protected
 *
 *
 */

package com.rostyslavprotsiv;

import com.rostyslavprotsiv.view.runner.Runner;

/**
 * {@code Main} class to execute whole program
 *
 * @version: 1.0.1
 * @author Rostyslav Protsiv
 * @since 06.08.2021
 * @see Object
 */
public final class Main {
    /* Entry point */
    /**
     * Private constructor that is supposed to be never used
     */
    private Main() {
        throw new UnsupportedOperationException();
    }

    /**
     * Entry point for the program
     * @param args Params to be passed throughout command line
     */
    public static void main(String[] args) {
        Runner.run();
    }
}
