/*
 * Runner class, it is connecting output and business logic classes
 *
 * Version: 1.0.1
 *
 * Created by Rostyslav Protsiv, Date: 06.08.2021
 *
 * Copyright protected
 */

package com.rostyslavprotsiv.view.runner;

import com.rostyslavprotsiv.action.RangeAction;
import com.rostyslavprotsiv.entity.Range;
import com.rostyslavprotsiv.exception.RangeLogicalException;
import com.rostyslavprotsiv.view.Menu;

/**
 * {@code Runner} class that is layer between business logic and client connection
 *
 * @author Rostyslav Protsiv
 * @version 1.0.1
 * @since 06.08.2021
 */
public final class Runner {
    /**
     * Private constructor that is supposed to be never used
     */
    private Runner() {
        throw new UnsupportedOperationException();
    }

    /**
     * Static method that asks user to input some values and initiates with them
     * Range object
     *
     * @throws RangeLogicalException if something with min or max values is bad,
     * this exception will be thrown
     *
     * @see Range
     */
    private static Range getRange() throws RangeLogicalException {
        Range range;
        int min;
        int max;
        Menu.askForInput();
        min = Menu.askToInputFirst();
        max = Menu.askToInputSecond();
        range = new Range(min, max);
        return range;
    }

    /**
     * Private method to print and get odd numbers
     * @param range Range of numbers
     */
    private static void getAndPrintOddNumbers(Range range) {
        int numbers[] = RangeAction.getOddFromStartToEnd(range);
        Menu.printOddNumbers(numbers);
    }

    /**
     * Private method to print and get even numbers
     * @param range Range of numbers
     */
    private static void getAndPrintEvenNumbers(Range range) {
        int numbers[] = RangeAction.getEvenFromEndToStart(range);
        Menu.printEvenNumbers(numbers);
    }

    /**
     * Private method to get and print sum of all odd numbers
     * @param range Range of numbers
     */
    private static void getAndPrintOddSum(Range range) {
        int sum = RangeAction.getOddSum(range);
        Menu.printSumOfOddNumbers(sum);
    }

    /**
     * Private method to get and print sum of all even numbers
     * @param range Range of numbers
     */
    private static void getAndPrintEvenSum(Range range) {
        int sum = RangeAction.getEvenSum(range);
        Menu.printSumOfEvenNumbers(sum);
    }

    /**
     * This is the main class that describes the algorithm of this application
     */
    public static void run() {
        try {
            Range range = getRange();
            getAndPrintOddNumbers(range);
            getAndPrintEvenNumbers(range);
            getAndPrintOddSum(range);
            getAndPrintEvenSum(range);
        } catch(RangeLogicalException rle) {
            System.err.println(rle.getCause() + " : " + rle.getMessage());
        }
    }
}
