/*
 * Menu class
 *
 * Version: 1.0.1
 *
 * Created by Rostyslav Protsiv, Date: 06.08.2021
 *
 * Copyright protected
 */
package com.rostyslavprotsiv.view;

import java.util.Scanner;

/**
 * <h1>{@code Menu}</h1> is a class to display client Menu
 *
 * @version 1.0.1
 * @author Rostyslav Protsiv
 * @since 06.08.2021
 */
public final class Menu {
    /* This class helps program to display user menu */

    /**
     * Private constructor that is supposed to be never used
     */
    private Menu() {
        throw new UnsupportedOperationException();
    }

    /**
     * Scanner variable that enable user to input values
     *
     * @see Scanner
     */
    private static final Scanner SCANNER = new Scanner(System.in);

    /**
     * Method asks to input numbers from the keyboard
     *
     * @see System
     */
    public static void askForInput() {
        System.out.println("Please enter the range of the values : ");
    }

    /**
     * Method asks to input minimum value for the range from the keyboard
     *
     * @see System
     * @return The value input from the keyboard
     */
    public static int askToInputFirst() {
        int firstN;
        System.out.println("Please enter the first(minimum value) : ");
        firstN = SCANNER.nextInt();
        return firstN;
    }

    /**
     * Method asks to input maximum value for the range from the keyboard
     *
     * @see System
     * @return The value input from the keyboard
     */
    public static int askToInputSecond() {
        int secondN;
        System.out.println("Please enter the second(maximum value) : ");
        secondN = SCANNER.nextInt();
        return secondN;
    }

    /**
     * This method prints odd numbers to the console
     * @param n Numbers to be printed
     */
    public static void printOddNumbers(int[] n) {
        for(Integer number: n) {
            System.out.println("Odd: " + number);
        }
    }

    /**
     * This method prints even numbers to the console
     * @param n Numbers to be printed
     */
    public static void printEvenNumbers(int[] n) {
        for(Integer number: n) {
            System.out.println("Even: " + number);
        }
    }
    /**
     * This method prints sum of odd numbers to the console
     * @param sum Sum to be printed
     */
    public static void printSumOfOddNumbers(int sum) {
        System.out.println("Sum of odd numbers: " + sum);
    }

    /**
     * This method prints sum of even numbers to the console
     * @param sum Sum to be printed
     */
    public static void printSumOfEvenNumbers(int sum) {
        System.out.println("Sum of even numbers: " + sum);
    }

    /**
     * The method asks user to input the Fibonacci number
     * @return Number of Fibonacci elements
     */
    public static int askToInputFibonacciNumber() {
        int number;
        System.out.println("Please input the number of Fibonacci elements");
        number = SCANNER.nextInt();
        return number;
    }
}
